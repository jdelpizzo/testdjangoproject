echo -e "\e[34mLinting\e[0m"
COUNTER=5
SONAR_ERR=1
echo -e "\e[34m  Running sonar scanner\e[0m"\
echo -e "\e[31m  YOU ARE PUTTING THE SCANNER AUTH IN THE PIPELINE. SWITCH IT TO A GITLAB SECRET\e[0m"
if AUTH=0696f27b5c0931ab12748089c4d6d108474684dc docker-compose up sonar-scanner-cli &> ./sonarscanner.log
then SONAR_ERR=$?; 
else SONAR_ERR=$?;
fi
AUTH=""
echo -e "\e[33m  Sonar scanner exited with $SONAR_ERR\e[0m";
until [[ "$SONAR_ERR" -eq "0" ]]; do
  if (($COUNTER < 1 ))
    then
      echo -e "\e[31m  SONAR SCANNER COULD NOT SCAN! and exited with error code $SONAR_ERR\e[0m"
      echo -e "\e[31m  The log of the last run is located in ${pwd}sonarscanner.log\e[0m"
      exit 1
    fi
  echo -e "\e[33m  SONAR SCANNER FAILED! Sonarqube is probably not up\e[0m";
  ((COUNTER--))
  echo -e "\e[33m  sleeping for 30 seconds before trying again, will try $COUNTER more times\e[0m"
  sleep 30
  echo -e "\e[33m  running sonar scanner\e[0m"
  echo -e "\e[31m  YOU ARE PUTTING THE SCANNER AUTH IN THE PIPELINE. SWITCH IT TO A GITLAB SECRET\e[0m"
  if AUTH=0696f27b5c0931ab12748089c4d6d108474684dc docker-compose up sonar-scanner-cli &> ./sonarscanner.log
  then SONAR_ERR=$?; 
  else SONAR_ERR=$?;
  fi
  AUTH=""
echo -e "\e[33m  Sonar scanner exited with $SONAR_ERR\e[0m";
done
echo -e "\e[34m  Sonar Scanner completed succesfully\e[0m"
echo -e "\e[34m  The log of the last Sonar Scanner run is located in ${pwd}sonarscanner.log\e[0m"